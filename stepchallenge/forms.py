from dal import autocomplete
from django import forms
from django.contrib.auth.forms import UserCreationForm, ReadOnlyPasswordHashField, UsernameField
from django.contrib.auth.models import User
from django.conf import settings

from bootstrap_datepicker.widgets import DatePicker

import datetime
from .models import Team, Profile

class UpdateEmailSettings(forms.ModelForm):
    email = forms.EmailField(max_length=254, help_text='Required. Please provide a valid email address.')
    email_optin = forms.BooleanField(required=False, initial=True, label="I'd like to receive emails about the Python March competition.")

    class Meta:
        model = User
        fields = ('email', 'email_optin', )

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Please provide a valid email address.')
    privacy_agreed = forms.BooleanField(required=True, label="I agree to the <a href=\"/privacy\">privacy policy</a> in effect.")
    email_optin = forms.BooleanField(required=False, initial=True, label="I'd like to receive emails about the Python March competition.")

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'privacy_agreed', 'email_optin', )

class StepForm(forms.Form):
    steps = forms.IntegerField(label="Steps taken", min_value=1)

class PastStepForm(StepForm):
    step_date = forms.DateField(label="Date stepped", 
          widget=DatePicker(options={"format": "yyyy-mm-dd",
                                     "todayHighlight": "true",
                                     "autoclose": "true",
                                     "calendarWeeks": "true",
                                     "startDate": settings.FOSSWALK_START_DATE,
                                     "endDate": datetime.date.today().isoformat(),
                                     "weekStart": 1}))

def get_choice_list():
   return [t.team_name for t in Team.objects.filter(team_lead__isnull=False)]

class SelectTeam(forms.Form):
    team = autocomplete.Select2ListCreateChoiceField(
                 choice_list=get_choice_list,
                 required=True,
                 label="New team",
                 widget=autocomplete.ListSelect2(url='team-autocomplete'))

class NewTeam(forms.Form):
   team_name = forms.CharField(label="Team name")
