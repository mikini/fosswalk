from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Team(models.Model):
    team_name = models.CharField(max_length=200)
    team_lead = models.ForeignKey(User, related_name='lead', blank=True, null=True)

    def __str__(self):
        return self.team_name

    class Meta:
        ordering = ['-team_name']

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, related_name='user', blank=True, null=True)
    email_confirmed = models.BooleanField(default=False)
    privacy_agreed = models.BooleanField(default=False)
    email_optin = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

class StepCount(models.Model):
    user = models.ForeignKey(User, related_name='stepcount')
    steps = models.IntegerField(default=0)
    step_date = models.DateField()

    def __str__(self):
        return "%s steps by %s on %s" % (self.steps, self.user, self.step_date)
