from django.apps import AppConfig


class StepchallengeConfig(AppConfig):
    name = 'stepchallenge'
