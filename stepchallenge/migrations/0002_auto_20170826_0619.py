# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-26 04:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('stepchallenge', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='team',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user', to='stepchallenge.Team'),
        ),
    ]
