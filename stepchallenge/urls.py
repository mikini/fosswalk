from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

from . import views
from .autocomplete import TeamAutocomplete

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^password_reset/update/$', views.change_password, name='change_password'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),

    url(r'^account_activation_sent/$', views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),

    url(r'^signup/$', views.signup, name='signup'),
    url(r'^register$', login_required(views.register), name='register'),
    url(r'^register_past$', login_required(views.register_past), name='register_past'),
    url(r'^newteam$', login_required(views.newteam), name='newteam'),
    url(r'^jointeam/$', login_required(views.jointeam), name='jointeam'),
    url(r'^team/(?P<team_id>[0-9]+)$', views.team_detail, name='team'),
    url(r'^walker/(?P<user_id>[0-9]+)$', views.user_detail, name='user'),
    url(r'^team-autocomplete/$', TeamAutocomplete.as_view(), name='team-autocomplete'),
    url(r'^privacy/$', TemplateView.as_view(template_name="privacy.html"), name='privacy'),
    url(r'^contact/$', TemplateView.as_view(template_name="contact.html"), name='contact'),
    url(r'^user/update/$', login_required(views.updateemailsettings), name='updatesettings'),
]
