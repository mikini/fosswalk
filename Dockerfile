FROM python:3.6
ARG ENV=prod

COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 8000
RUN python manage.py collectstatic --noinput

CMD python manage.py migrate && gunicorn -b 0.0.0.0:8000 --log-level debug fosswalk.wsgi:application
